from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import User

import datetime as d


class Subject(models.Model):
    subject_name = models.CharField(max_length=50)

    def __str__(self) -> str:
        return self.subject_name
    
class Question(models.Model):
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE, related_name = 'subject')
    question_text = models.CharField(max_length=254 )
    marks = models.IntegerField(default=5)

    def __str__(self) -> str:
        return self.question_text
    
class Answer(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE , related_name='question')
    ans_text = models.CharField(max_length=254, )
    is_correct = models.BooleanField()

    def __str__(self) -> str:
        return self.ans_text

class Result(models.Model):
    subject = models.CharField(max_length=50)
    total_question = models.IntegerField()
    marks = models.IntegerField()

    def __str__(self) -> str:
        return self.subject
