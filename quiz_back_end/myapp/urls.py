from django.urls import path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()

urlpatterns = [
    path('', views.index, name='index'),

    path('show-subjects/', views.showSubject, name='show-subjects'),

    path('show-questions/', views.showQuestion, name='show-questions'),

    path('create-result', views.createResult, name='create-result'),

    path('show-result/<str:subject>/', views.showResult, name='show-result'),

    path('show-detail-result/', views.showDetailResult, name='show-detail-result')


]