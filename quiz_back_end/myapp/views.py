from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from myapp.serializers import *
from rest_framework.authentication import BasicAuthentication, SessionAuthentication
from rest_framework.permissions import IsAuthenticated
import random , json

# Create your views here.

@api_view(['GET'])
def index(req):
    return Response({'greeting':'Welcome to the quiz competition'}, status=status.HTTP_200_OK)

@api_view(['GET'])
# @authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
def showSubject(req):
    subjects = Subject.objects.all()
    serializer = SubjectSerializer(subjects, many=True)

    return Response(serializer.data, status=status.HTTP_200_OK)

@csrf_exempt
@api_view(['GET', 'POST'])
def showQuestion(req):
    subject = req.GET.get('subject')
    cnt = int(req.GET.get('cnt'))
    subject = Subject.objects.get(subject_name = subject)
    questions = list(Question.objects.filter(subject=subject))[:cnt]
    random.shuffle(questions)
    # json_data["question"] = json.dumps(serializer.data, ensure_ascii=False)

    answerTextList = []
    response_obj = []
    for q in questions:
        answer = Answer.objects.filter(question=q)
        answerList = AnswerSerializer(answer, many=True).data
        questionText = QuestionSerializer(q).data

        
        for al in answerList:
            answerTextList.append({"ans_text":al['ans_text'], "is_correct":al['is_correct']})
        
        response_obj.append({
            'id': questionText['id'],'question':questionText['question_text'],'marks':questionText['marks'], 'answers':answerList,
            })

    return Response(response_obj,  status=status.HTTP_200_OK)

@csrf_exempt
@api_view(['GET','POST'])
def createResult(req):
    if req.method == 'POST':
        data = req.data

        serializer = ResultSerializer(data=data)
        if serializer.is_valid(raise_exception=True):
            print('success', serializer.data)

            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            print('failed')

    return Response({'done':'ok'}, status=status.HTTP_200_OK)



@csrf_exempt
@api_view(['GET', 'POST'])
def showResult(req, subject):

    try:
        result = Result.objects.get(subject=subject)
    except Result.DoesNotExist:
        return Response({'msg':'Result not found'}, status=status.HTTP_404_NOT_FOUND)
    
    serializer = ResultSerializer(result)
    return Response(serializer.data, status=status.HTTP_200_OK)



@csrf_exempt
@api_view(['GET', 'POST'])
def showDetailResult(req):
    querydata = req.GET.get('querydata')
    querydata = json.loads(querydata)
    response_obj = []

    for id in querydata:
        question = Question.objects.get(pk = id)
        answers = Answer.objects.filter(question = question)

        question_serializer = QuestionSerializer(question).data
        answer_serializer = AnswerSerializer(answers, many=True).data

        answerList = []

        for ans in answer_serializer:
            answerList.append({
                "id":ans['id'],
                "answer":ans['ans_text'],
                "is_correct":ans['is_correct'],
            })

        response_obj.append({
            "id":question_serializer['id'],
            "question":question_serializer['question_text'],
            "answers":answerList,
        })

    return Response(response_obj, status=status.HTTP_200_OK)


