from django.urls import path
from . import views 

urlpatterns = [
    path('', views.index, name='index'),

    path('create-question/', views.createQuestion, name='create-question'),
    
    path('show-subjects/', views.showSubject, name='show-subjects'),

    path('instruction', views.showInstruction, name='instruction'), 


    path('submit-answer', views.submitAnswer, name='submit-answer'),

    path('show-questions/', views.showQuestion, name='show-questions'),


    path('show-result/', views.showResult, name='show-result'),

    path('view-detail-result', views.viewDetailResult, name='view-detail-result'),

]