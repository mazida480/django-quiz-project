from typing import Any
from django.http import JsonResponse
from django.shortcuts import render, HttpResponseRedirect, HttpResponse
from django.urls import reverse
import requests
from . forms import *
from django.contrib import messages
from django.core.paginator import Paginator
from django.forms import formset_factory

# Create your views here.


# Show home page.
def index(req):
    response = requests.get('http://localhost:3000/')
    response = response.json()
    context = {'greeting':response['greeting']}

    return render(req, 'myapp/home.html', context)

# Form for creating questions.
def createQuestion(req):
    context = {}
    if req.method == 'POST':
        form = QuestionForm(req.POST)

        if form.is_valid():
            response = requests.post('http://localhost:3000/create-question/', data=req.POST)

            response = response.json()

            messages.success(req, response['msg'])
            return HttpResponseRedirect(reverse('create-question'))
        else:
            form = QuestionForm(req.POST)
            context['form'] = form
            return render(req, 'myapp/admin/create-question.html', context)
    else:
        form  = QuestionForm()
        context['form'] = form
        return render(req, 'myapp/admin/create-question.html', context)

# Sujects included to the quiz.
def showSubject(req):
    subjects = getAllSubjects()
    print(subjects)    
    context = {'subjects':subjects}
    return render(req, 'myapp/quizzes.html', context)


# Show instruction for particular subject.
def showInstruction(req):
    subjects = getAllSubjects()
    subject = req.GET.get('subject')
    id = req.GET.get('id')
    context = {'subject':subject, 'id':id, 'all_subjects':subjects}
    
    return render(req, 'myapp/instruction.html', context)



def submitAnswer(req):
    context = {}
    questions = requests.get('http://localhost:3000/warmup-question/{sub}'.format(sub='Maths'))

    questions = questions.json()
    context['questions'] = questions
    
    return render(req, 'myapp/answer.html', )
        

import json
def showQuestion(req):
    
    if req.method == 'POST':

        data = req.POST
        
        context = {'data':data}
        context = json.dumps(context)

        response = requests.post('http://localhost:3000/create-result', data = data)

        response = response.json()

        req.session['result'] = context


        return JsonResponse({'result':context, 'msg':'Answers submitted successfully'})

        

    response = requests.get('http://localhost:3000/show-questions/?subject={0}&cnt={1}'.format(req.GET.get('subject'), req.GET.get('cnt')))

    response = response.json()

    context = {'list_obj':response}

    return render(req, 'myapp/submit-answer.html', context)


def showResult(req):
    # subject = req.GET.get('subject')
    # response = requests.get('http://localhost:3000/show-result/{s}'.format(s=subject))

    # response = response.json()
    # context = {'result':response}
    # print(context['result'])
    result = json.loads(req.session.get('result'))
    context = {'result':result}
    return render(req, 'myapp/user/result.html',context)

# Utility function get all subjects.
def getAllSubjects():
    response = requests.get('http://localhost:3000/show-subjects/')

    response = response.json()

    return response


def viewDetailResult(req):
    result = req.session.get('result')
    result = json.loads(result)
    question_ids = result['data']['question_ids']

    print(question_ids)

    response = requests.get('http://localhost:3000/show-detail-result/?querydata={d}'.format(d=question_ids))

    response = response.json()

    print(response)
    
    context = {'result':response}

    return render(req, 'myapp/user/view-detail-result.html',context)
    