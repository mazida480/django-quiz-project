from typing import Any
from django.contrib.auth.models import User
from django import forms
from . import models



class ResultForm(forms.Form):
    subject = forms.CharField(max_length=50)
    total_question = forms.IntegerField()
    marks = forms.IntegerField()

class QuestionForm(forms.Form):
    question = forms.CharField(max_length=254)